# PracticaFinal

Entrega de la práctica final del curso en Programación en Unity 3d de la UOC

Juego en 3a persona estilo realista ambientado en una ciudad amenazada por zombies. El player deberá explorar la ciudad (estilo mundo abierto) para encontrar y matar los zombies que
patrullan por las calles. El jugador tendrá 8 min para superar la misión/juego. En la ciudad deberá ir con cuidado con los peatones y los coches que circulan a sus anchas y podrá
circular con su própio coche. El escenario, la ambientación y la música del juego estan inspirados a juegos como los de la saga GTA, Crazy Taxy y Driver.

![render_menu](https://user-images.githubusercontent.com/49591290/60384589-f563e700-9a7f-11e9-87df-b93e810c15f3.jpg)


# Como jugar
Descargar y extraer el fichero.zip y ejecutar el fichero "CrazyCity.exe" dentro de la carperta Build. 


# Controles de juego
Para el movimiento del player/coche se pueden usar las flechas o las teclas WASD.

Para saltar pulsar la "barra espaciadora".

Para atacar con la katana pulsar la tecla "K".

Para entrar en el coche pulsar la tecla "I". Para salir del coche pulsar la tecla "O".

Pulsar la tecla "Esc" para ir al menú principal.

![render_controls](https://user-images.githubusercontent.com/49591290/60384669-a5d1eb00-9a80-11e9-8cc3-3545009d8e8a.jpg)





# Puntos básicos de programación que se han implementado
1. **Player**
*  Se han utilizado los Scripts **ThirdPersonCharacter** y **ThirdPersonUserControl** para el movimiento del personaje en tercera persona. Se han asociado las animaciones de correr y salto 
*  del personaje a los inputs vertical/horizontal y barra espaciadora. Para el ataque del personaje con la espada/katana se ha utilizado el script **PlayerAttack** con el input del teclado 
*  de la letra "K".
*  Script **InCar** para la funcionalidad del player con el vehiculo. En este caso se ha implementado el uso de las teclas "I" y "O" para entrar y salir del coche respectivamente.
*  Script **PlayerHealth** para tener visible la vida del player y recibir daño. En este caso se tenido en cuenta el slider de unity para la barra de vida del personaje. La cantidad de vida 
*  también va medida por integers.

2. **Enemigos**
*  Para la implementación de los zombies enemigos se han tenido en cuenta varios Scripts con funcionalidades distintas:
*  Script **ZombiesIA** se ha utilizado para cada uno de los enemigos y controla su movimiento en la escena. Éste script tiene en cuenta la posición (transformada) de varios wayponts
*  para que los enemigos vayan hacia ellos y así hacer un recorrido. El Script recoge el componente NavMeshAgent de cada uno de los enemigos y su movimiento hacia el player se detecta cuando
*  éste entra en colisión gracias al script **ZombiesToPlayer** que recoge y pasa la transformada del player para que los zombies puedan perseguirlo.
*  Script **ZombiesAttack** se utiliza para activar el ataque de cada uno de los enemigos, mediante trigger junto con las animaciones correspondientes. En éste script también se han
*  usado integers para establecer el daño de los enemigos. 
*  Script **DestroyEnemy** se utiliza para destruir cada uno de los enemigos junto a sus respectivas partículas y audio asociado. En este script se ha implementado una función que destruye y 
*  hace desaparecer el enemigo para mejorar el rendimiento del juego.
*  El Script **CompleteGame** se utilizan solamente para determinar si el Player ha derrotado a les 12 zombies que hay en juego y así poder ganar la partida.
*  
3. **Otros Scripts que se han implementado**
*  En esta parte se ha utilizado varios Scripts para determinar las IA de los humanos y coches que circulan por la ciudad.
*  Así pues para los vehículos se han utilizado los scripts **CarEngine**, **CarWheel** y **Path** para determinar los puntos por los que va pasar cada uno de los coches, velocidades, puntos
*  de fricción etc.
*  Para los humanos se han utilizado los scripts **HumanIA** y **Path** para determinar los waypoints por los que pasarian cada uno de los humanos gracias tambien al componente NavMeshAgent.
*  Para determinar los puntuación obtenida tras derrotar cada uno de los zombies se ha utilizado los scripts **ScoreManager** y parte del **DestroyEnemy**.
*  Para la cuenta atrás del tiempo que se muestra en pantalla (para completar el juego) se ha utilizado el script **CountDown** el cuál te leva a la pantalla de GameOver en
*  caso que el tiempo llegue a 0 y el player no haya derrotado a todos los zombies.
*  
4. **Controles de pantallas**
*  Por lo que refiere a la navegación de las pantallas del menu principal y menu dentro del juego se han utilizado los scripts **Menu** y **MenuInGame**.
*  El Script **SettingsMenu** se ha implementado para controlar las opciones relacionadas con el volumen del juego, resolución de pantalla y calidad de los gráficos.
 

# Puntos extra que se han implementado
1. **Mapa**: Como uno de los puntos extra se ha implementado un mapa en la GUI del juego para determinar en todo momento la posición del player y la posición de todos los zombies enemigos.
Es una caracteristica que tienen todos los juegos estilo mundo abierto de grandes magnitudes.
2. **Atropello de zombies**: Se ha tenido en cuenta el hecho que el player podrá atropellar y destruir los zombies con su coche y así poder completar la misión más rápido.
3. **Katana**: En éste juego se ha optado por el combate cuerpo a cuerpo y así diferenciarse de los shooteers empleados en las ortas prácticas. Para ello se ha dispuesto de una pequeña samurai
como personaje principal, que armada con una katana esta dipuesta a poner fin a la amenaza zombie. Personaje muy inspirado a KillBill o Michone de la série "The Walking Dead".

![render_game](https://user-images.githubusercontent.com/49591290/60385397-0402cc00-9a89-11e9-9459-79676d917b54.jpg)

4. **Musicas y Efectos de Sonido**: Para generar la ambientación de acción y aventura que se buscava desde un incio se han utilizado soundtracks estilo acción y efectos de sonido
parecidos a juegos referentes como GTA, Driver y Crazy Taxi. 
5. **Partículas y Skybox**: Como aspectos visuales se ha optado de canviar la default Skybox de Unity por otra Skybox con cielo más adiente para el juego al igual que
se ha generado varios sistemas de partículas para crear las salpicaduras de sangre de los zombies al morir. Las partículas ayudan a generar más dinamismo y immersión en el juego.

# Link al video de gameplay del juego y comentarios de lo que se ha implementado
[Video Gameplay Crazy City (Práctica Final)](https://www.youtube.com/watch?v=X9HjHwpinZM)
